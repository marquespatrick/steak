﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLog;
using Steak.Monitoring.Data;
using Steak.Data.NoSQL;

namespace Steak.LogViewer.Controllers
{
    public class HomeController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public ActionResult Index()
        {

            LogRepositoryRavenDB logRepo = new LogRepositoryRavenDB(Helper.GetSession());
            var Logs = logRepo.GetLogs();
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            //int boucle = 500;

            //for (int i = 0; i < boucle; i++)
            //{
            //    try
            //    {
            //        var x = 23;
            //        var y = 0;
            //        var z = x / y;
            //    }
            //    catch (Exception e)
            //    {

            //        logger.Error(e);
            //    }

            //}

            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
