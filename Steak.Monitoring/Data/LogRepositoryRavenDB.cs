﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Steak.Monitoring.Target;
using Raven.Client;
using Steak.Monitoring.Core;
using NLog;


namespace Steak.Monitoring.Data
{
    public class LogRepositoryRavenDB
    {
        private IDocumentSession session;

        public LogRepositoryRavenDB(IDocumentSession session)
        {
            this.session = session;

        }
        public Log Load(int id)
        {
            return session.Load<Log>(id);
        }



        public IEnumerable<LogEventInfo> GetLogs()
        {
            LogEventInfo test = new LogEventInfo();
            var logs = session.Query<LogEventInfo>()
         .ToArray();
            return logs;
        }
        public void Save(LogEventInfo log)
        {
            session.Store(log);
            session.SaveChanges();
        }

        //public IEnumerable<LogEventInfo> GetLogs()
        //{
        //    LogEventInfo test = new LogEventInfo();
        //    var logs = session.Query<LogEventInfo>()
        // .ToArray();
        //    return logs;
        //}
        //public void Save(LogEventInfo log)
        //{
        //    session.Store(log);
        //    session.SaveChanges();
        //}

        public void Delete(int id)
        {
            var log = Load(id);
            session.Delete<Log>(log);
            session.SaveChanges();
        }

        
    }
}
