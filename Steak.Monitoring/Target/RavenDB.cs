﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using NLog.Targets;
using Steak.Monitoring.Data;
using Raven.Client;
using Raven.Client.Document;
using Steak.Monitoring.Core;

namespace Steak.Monitoring.Target
{

    [Target("RavenDB")]
    public sealed class RavenDB : TargetWithLayout
    {

        public string Url { get; set; }
        private static DocumentStore documentStore;

        public RavenDB(string url)
        {
            this.Url = url;
            documentStore = new DocumentStore { Url = this.Url };
            documentStore.Initialize();
        }
        ~RavenDB()
        {
            documentStore.Dispose();
        }
        protected override void Write(LogEventInfo logEvent)
        {
            LogEventInfo t = new LogEventInfo();

            Log log = new Log();
            SendLogToRavenDB(this.Url, log);


        }

        private void SendLogToRavenDB(string url, Log log)
        {
            LogRepositoryRavenDB repository = new LogRepositoryRavenDB(documentStore.OpenSession());
            repository.Save(log);
        }
        //private void SendLogToRavenDB(string url, LogEventInfo log)
        //{
        //    LogRepositoryRavenDB repository = new LogRepositoryRavenDB(documentStore.OpenSession());
        //    repository.Save(log);
        //}
    }
}
