﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Steak.Data.SQL
{
    public class CRUD
    {

        public int Id = -1;

        private string sql;
        private string tableName
        {
            get { return this.GetType().Name.ToUpper(); }

        }


        public void Load()
        {

            if (Id != -1)
            {
                using (SqlConnection connection = Helper.GetConnection())
                {
                    using (SqlCommand command = connection.GetCommand("SELECT * FROM " + tableName + " WHERE Id = @id;", CommandType.Text))
                    {
                        command.AddParameter("@id", this.Id, SqlDbType.Int);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                           
                            while (reader.Read())
                            {
                                foreach (var field in this.GetType().GetFields().ToArray())
                                {
                                    field.SetValue(this, reader[field.Name]);
                                }
                            }
                          
                        }

                    }
                }
            }
        }
        public void Save()
        {

            if (Id != -1)
            {
                sql = "UPDATE " + tableName + " SET ";
                foreach (var field in this.GetType().GetFields().ToArray()) if (field.Name != "Id") sql += field.Name + " = '" + field.GetValue(this) + "',";
                sql = sql.TrimEnd(',') + " WHERE Id = " + Id + ";";
            }
            else
            {
                sql = "INSERT INTO " + tableName + " (";
                foreach (var field in this.GetType().GetFields().ToArray()) if ( field.Name != "Id" ) sql += field.Name + ",";
                sql = sql.TrimEnd(',') + ") VALUES (";
                foreach (var field in this.GetType().GetFields().ToArray()) if (field.Name != "Id") sql += "'" +  field.GetValue(this) + "',";             
                sql = sql.TrimEnd(',') + ");";
            }
            using (SqlConnection connection = Helper.GetConnection())
            {
                using (SqlCommand command = connection.GetCommand(sql, CommandType.Text))
                {
                    command.ExecuteNonQuery();
                }
            }
        }
        public void Delete()
        {
            if (Id != -1)
            {
                sql = "DELETE FROM " + tableName + " WHERE Id = @id;";
                using (SqlConnection connection = Helper.GetConnection())
                {
                    using (SqlCommand command = connection.GetCommand(sql, CommandType.Text))
                    {
                        command.AddParameter("@id", this.Id, SqlDbType.Int);
                        command.ExecuteNonQuery();
                    }
                }

            }
        }
    }
}
