﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client;
using Raven.Client.Document;

namespace Steak.Data.NoSQL
{
    public static class Helper
    {

        private static string _url = "http://127.0.0.1:8080";

        private static DocumentStore documentStore;

        public static IDocumentSession GetSession()
        {
            return GetSession(_url);
        }

        public static IDocumentSession GetSession(string url)
        {
            try
            {
                documentStore = new DocumentStore { Url = url };
                documentStore.Initialize();
            }
            catch (Exception ex)
            {
                if (documentStore != null)
                {
                    documentStore.Dispose();
                }
            }
            return documentStore.OpenSession();
        }

    }
}
