﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using NLog.Targets; 

namespace MyConsoleApplication
{
    [Target("MyFirst")]
    public sealed class MyFirstTarget : TargetWithLayout
    {
        public MyFirstTarget()
        {
            this.Host = "localhost";
        }

        public string Host { get; set; }

        protected override void Write(LogEventInfo logEvent)
        {
            string logMessage = this.Layout.Render(logEvent);

            SendTheMessageToRemoteHost(this.Host, logMessage);
        }

        private void SendTheMessageToRemoteHost(string host, string message)
        {
            // TODO - write me 
        }
    } 
}
