﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Steak
{
    public class Composite
    {
        private List<BaseObject> _items = new List<BaseObject>();
        public List<BaseObject> Items
        {
            get { return _items; }
            set { _items = value; }
        }

    }
}
