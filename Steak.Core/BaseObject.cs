﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Steak
{
    public class BaseObject : Composite
    {
        string _wording;
        public string Wording
        {
            get { return _wording; }
            set { _wording = value; }
        }
    }

}
